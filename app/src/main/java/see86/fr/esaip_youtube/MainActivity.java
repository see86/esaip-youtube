package see86.fr.esaip_youtube;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.DocumentsProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    String lien= null;

    class lienweb extends AsyncTask<Void, Integer, Long>{

        @Override
        protected Long doInBackground(Void... params) {
            try {
                Document document = Jsoup.connect("https://savedeo.com/download?url="+lien).get();
                Elements tbody = document.select("tbody");
                for(Element element : tbody)
                    for(Element enfant : element.children())
                        if(enfant.select("th").get(0).text().equalsIgnoreCase("audio only") && enfant.select("td").get(0).text().equalsIgnoreCase("webm")){
                            Log.i("test", enfant.select("th").get(0).text()+" "+enfant.select("td").get(0).text()+" "+enfant.select("a").get(0).attr("abs:href"));
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(enfant.select("a").get(0).attr("abs:href")));
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            ((DownloadManager)getSystemService(DOWNLOAD_SERVICE)).enqueue(request);
                            break;
                        }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Intent.ACTION_SEND.equals(getIntent().getAction()) && getIntent().getType() != null) {
            if ("text/plain".equals(getIntent().getType())) {
                lien = getIntent().getExtras().getString(Intent.EXTRA_TEXT);
                if(lien.matches("^https:\\/\\/youtu.be\\/.*")){
                        new lienweb().execute();


                    //Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    //startActivity(intent);
                }
            }
        }
        finish();
    }
}

